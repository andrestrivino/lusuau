<?php
    //we need to get our variables first
    $email_to = (isset($_POST['email_to'])) ? $_POST['email_to'] : 'trivino1901@gmail.com'; //the address to which the email will be sent
    $subject  = (isset($_POST['subject'])) ? $_POST['subject'] : 'Información de contacto página web Simetria';

    if (!isset($_POST['name']) || !isset($_POST['email']) || !isset($_POST['message']) ) {
        echo 'failed';
    } else {
        $name     =   $_POST['name'];
        $email    =   $_POST['email'];
        $message  = "<html><body style=\"font-family: 'Roboto'; font-size: 17px;\">";
        $message .= '<p style="font-style: italic;">El usuario:</p>';
        $message .= '<p style="line-height: 5%; padding-left: 10px;"><span style="font-weight: bold;">' . $name .'</span> se puso en contacto a través de la página web.</p>';
        $message .= '</br><p style="font-style: italic;">Mensaje:</p>';
        $message .= '<div style="margin-left: 20px; border: 0.5px; padding: 10px; border-color: azure; border-radius: 10px; border-style: groove;"><span>' . $_POST['message'] .'</span></div>';
        $message .= '</body></html>';
        /*the $header variable is for the additional headers in the mail function,
        we are asigning 2 values, first one is FROM and the second one is REPLY-TO.
        That way when we want to reply the email gmail(or yahoo or hotmail...) will know
        who are we replying to. */
        $headers  = "From: $email\r\n";
        $headers .= "Reply-To: $email\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        if(mail($email_to, $subject, $message, $headers)){
            echo 'sent'; // we are sending this text to the ajax request telling it that the mail is sent..
        }else{
            echo 'failed';// ... or this one to tell it that it wasn't sent
        }
    }
?>
