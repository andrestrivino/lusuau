var stage = document.querySelector('.stage_services');
var stage = document.querySelector('.stage_services');
var background = document.querySelector('.background');

var rotation = 0;
var indiceSlider = 0;

// document.querySelector('.button_prev').
// addEventListener('click', function () {return applyStyles(rotation += 90);});

// document.querySelector('.button_next').
// addEventListener('click', function () {return applyStyles(rotation -= 90);});

// ROTACION DE LA PRIMERA PAGINA

document.querySelector('.uno').
addEventListener('click', function () {return applyStyles(rotation += 0);});

document.querySelector('.dos').
addEventListener('click', function () {return applyStyles(rotation -= 90);});

document.querySelector('.tres').
addEventListener('click', function () {return applyStyles(rotation += 180);});

document.querySelector('.cuatro').
addEventListener('click', function () {return applyStyles(rotation += 90);});

// ROTACION DE LA SEGUNDA PAGINA

document.querySelector('.unob').
addEventListener('click', function () {return applyStyles(rotation += 90);});

document.querySelector('.dosb').
addEventListener('click', function () {return applyStyles(rotation -= 0);});

document.querySelector('.tresb').
addEventListener('click', function () {return applyStyles(rotation -= 90);});

document.querySelector('.cuatrob').
addEventListener('click', function () {return applyStyles(rotation += 180);});

// ROTACION DE LA TERCERA PAGINA

document.querySelector('.unoc').
addEventListener('click', function () {return applyStyles(rotation += 180);});

document.querySelector('.dosc').
addEventListener('click', function () {return applyStyles(rotation += 90);});

document.querySelector('.tresc').
addEventListener('click', function () {return applyStyles(rotation -= 0);});

document.querySelector('.cuatroc').
addEventListener('click', function () {return applyStyles(rotation -= 90);});

// ROTACION DE LA CUARTA PAGINA

document.querySelector('.unod').
addEventListener('click', function () {return applyStyles(rotation -= 90);});

document.querySelector('.dosd').
addEventListener('click', function () {return applyStyles(rotation += 180);});

document.querySelector('.tresd').
addEventListener('click', function () {return applyStyles(rotation += 90);});

document.querySelector('.cuatrod').
addEventListener('click', function () {return applyStyles(rotation -= 0);});


function applyStyles() {
  background.style.filter = 'hue-rotate(' + rotation + 'deg)';
  stage.style.transform = 'rotateZ(' + rotation + 'deg)';
}


function showStuff(mostrar, page, page2, page3) {
    document.getElementById(mostrar).style.display = 'block';
    document.getElementById(mostrar).style.opacity = '1';

    document.getElementById(page).style.opacity = '0';
    document.getElementById(page2).style.opacity = '0';
    document.getElementById(page3).style.opacity = '0';
    window.setTimeout(
      function removethis()
      {
        document.getElementById(page).style.display='none';
        document.getElementById(page2).style.display = 'none';
        document.getElementById(page3).style.display = 'none';
      }, 600);

}


